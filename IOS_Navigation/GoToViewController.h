//
//  GoToViewController.h
//  IOS_Navigation
//
//  Created by Bc_Ltf on 15/4/14.
//  Copyright (c) 2015年 Bc_ltf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoToViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (nonatomic,strong) NSString* textTitle;

@end
