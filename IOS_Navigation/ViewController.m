//
//  ViewController.m
//  IOS_Navigation
//
//  Created by Bc_Ltf on 15/4/8.
//  Copyright (c) 2015年 Bc_ltf. All rights reserved.
//

#import "ViewController.h"
#import "GoToViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"GoTo"])
    {
        GoToViewController *detailViewController = segue.destinationViewController;
            detailViewController.textTitle=@"你好";
            detailViewController.title = @"这是跳转页";
    }
}

@end
